from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import  Migrate, MigrateCommand
from datetime import timedelta

app = Flask(__name__)
app.config.from_object('config')
#app.config['SECRET_KEY'] = 'Elizabeth_Gabi_Luthai'
#app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=5)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

from .models import tables
from .controllers import default


